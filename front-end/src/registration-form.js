import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class RegistrationForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			firstName : "",
			lastName : "",
			npiNumber : "",
			businessAddress : "",
			telephoneNumber : "",
			emailAddress : "",
		};
		this.renderMessage = this.props.renderMessage;
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	render() {
		return (
			<div className="registration-form">
				<h3>REGISTRATION FORM</h3>
				<form onSubmit={this.handleSubmit}>
					<div>
						<label htmlFor="first-name">
							First Name
						</label>
						<input
							id="first-name"
							name="firstName"
							onChange={this.handleChange}
							value={this.state.firstName} 
							type="text"
							required
						/>
					</div>	
					<div>
						<label htmlFor="last-name">
							Last Name
						</label>
						<input
							id="last-name"
							name="lastName"
							onChange={this.handleChange}
							value={this.state.lastName}
							required
						/>
					</div>	
					<div>
						<label htmlFor="npi-number">
							NPI Number
						</label>
						<input
							id="npi-number"
							name="npiNumber"
							onChange={this.handleChange}
							value={this.state.npiNumber}
							required
						/>
					</div>
					<div>
						<label htmlFor="business-address">
							Business Address
						</label>
						<input
							id="business-address"
							name="businessAddress"
							onChange={this.handleChange}
							value={this.state.businessAddress}
							required
						/>
					</div>
					<div>
						<label htmlFor="telephone-number">
							Telephone Number
						</label>
						<input
							id="telephone-number"
							name="telephoneNumber"
							onChange={this.handleChange}
							value={this.state.telephoneNumber}
							type="tel"
							placeholder="954-594-2710"
							pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
							required
						/>
					</div>
					<div>
						<label htmlFor="email-address">
							Email Address
						</label>
						<input
							id="email-address"
							name="emailAddress"
							onChange={this.handleChange}
							value={this.state.emailAddress}
							type="email"
							required
						/>
					</div>
					<button>
						SUBMIT
					</button>		
				</form>
			</div>
		);
	}
	
	handleChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}
	
	handleSubmit(e) {
		e.preventDefault();
		
		const messageElement = (
			<p>{"User: " + this.state.lastName + ", " + this.state.firstName + " NPI Number: " + this.state.npiNumber + " Busines Address: " + this.state.businessAddress + " Telephone Number: " + this.state.telephoneNumber + " Email: " + this.state.emailAddress + " was register " + Date.now()}</p>
		);
		
		this.renderMessage(messageElement);
		
		Array.from(document.querySelectorAll("input")).forEach(input => (this.setState({ [input.name]: "" })));
	}
	
}

export default RegistrationForm;