import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import RegistrationForm from './registration-form.js';
import RegistrationMessage from './registration-message.js';

class Registration extends React.Component {
	constructor(props) {
		super(props);
		this.messageComponent = React.createRef();
		this.renderMessage = this.renderMessage.bind(this);
	}
	
	render() {
		return (
			<div className="registration">
				<RegistrationForm renderMessage={this.renderMessage}/>
				<RegistrationMessage ref={this.messageComponent}/>
			</div>
		);
	}
	
	
	renderMessage(messageElement){
		this.messageComponent.current.renderMessage(messageElement);
	}
}

ReactDOM.render(
  <Registration />,
  document.getElementById('root')
);

