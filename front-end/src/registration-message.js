import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class RegistrationMessage extends React.Component {
	render() {
		return (
			<div>
				<h3>REGISTRATION MESSAGE</h3>
				<p>You just registered a provider with this information: </p>
				<div id="providerInfo" />
			</div>
		);
	}
	
	renderMessage(messageElement){
		ReactDOM.render(messageElement, document.getElementById('providerInfo'));
	}

}

export default RegistrationMessage;