package com.yanet.enrollees;

import java.util.List;

public class Enrollee {

    private String userId;
    private String firstName;
    private String lastName;
    private int version;
    private String insuranceCompany;

    public Enrollee(List<String> fields) {
        userId = fields.get(0);
        firstName = fields.get(1);
        lastName = fields.get(2);
        version = Integer.valueOf(fields.get(3));
        insuranceCompany = fields.get(4);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getEnrolleeCSVFormatRow() {
        return userId + "," + firstName + "," + lastName + "," + version + "," + insuranceCompany;
    }

    @Override
    public String toString() {
        return "Enrollee{" +
                "userId='" + userId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", version=" + version +
                ", insuranceCompany='" + insuranceCompany + '\'' +
                '}';
    }
}
