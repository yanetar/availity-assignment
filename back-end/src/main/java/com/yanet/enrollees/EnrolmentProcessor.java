package com.yanet.enrollees;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EnrolmentProcessor {


    public List<Enrollee> sortEnrolleesByLastAndFirstName(List<Enrollee> enrollees) {
        return enrollees.stream().sorted(this::compareEnrolleesByLastAndFirstName).collect(Collectors.toList());
    }

    private int compareEnrolleesByLastAndFirstName(Enrollee e1, Enrollee e2) {
        int lastNameOrder = e1.getLastName().compareTo(e2.getLastName());
        return (lastNameOrder == 0) ? e1.getFirstName().compareTo(e2.getLastName()): lastNameOrder;
    }

    public List<Enrollee> removeDuplicateUserIds(List<Enrollee> enrollees) {
        Map<String, List<Enrollee>> enrolleeMap = enrollees
                .stream()
                .collect(Collectors.groupingBy(Enrollee::getUserId));
        return enrolleeMap
                .entrySet()
                .stream()
                .map(entry -> keepEnrolleesWithHighestVersion(entry.getValue()))
                .collect(Collectors.toList());
    }

    private Enrollee keepEnrolleesWithHighestVersion(List<Enrollee> enrollees) {
       return enrollees
                .stream()
                .max(Comparator.comparing(Enrollee::getVersion))
                .get();
    }

    public Map<String, List<Enrollee>> splitByInsurance(List<Enrollee> enrollees) {
        return enrollees.stream().collect(Collectors.groupingBy(Enrollee::getInsuranceCompany));
    }
}
