package com.yanet.enrollees;

import java.util.List;

public class EnrollmentOrchestrator {

    private EnrolmentProcessor enrolmentProcessor = new EnrolmentProcessor();
    private EnrollmentFileIo enrollmentFileIo = new EnrollmentFileIo();

    public void processEnrollmentCsv(String csvFile, String outputFolder) throws Exception {
        List<Enrollee> allEnrollees = enrollmentFileIo.readEnrollmentFile(csvFile);
        enrolmentProcessor.splitByInsurance(allEnrollees)
                .entrySet()
                .stream()
                .map(entry -> enrolmentProcessor.removeDuplicateUserIds(entry.getValue()))
                .map(insEnrollees -> enrolmentProcessor.sortEnrolleesByLastAndFirstName(insEnrollees))
                .forEach(insEnrollees -> enrollmentFileIo.writeEnrolleesInFile(insEnrollees, outputFolder));
    }

}
