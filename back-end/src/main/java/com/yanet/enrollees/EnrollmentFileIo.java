package com.yanet.enrollees;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EnrollmentFileIo {

    public List<Enrollee> readEnrollmentFile(String fileName) throws Exception {
        Stream<String> lines = Files.lines(Paths.get(fileName));
        return lines
                .filter(line -> !line.contains("User Id"))
                .map(line -> new Enrollee(Arrays.asList(line.split(","))))
                .collect(Collectors.toList());
    }

    public void writeEnrolleesInFile(List<Enrollee> enrollees, String outputFolder) {
        try {
            Path path = Paths.get(outputFolder + enrollees.get(0).getInsuranceCompany() + ".csv");
            Files.deleteIfExists(path);
            Path file = Files.createFile(path);

            Files.write(file, ("User Id,First Name,Last Name,Version,Insurance Company" + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);

            for (Enrollee enrollee : enrollees) {
                Files.write(file, (enrollee.getEnrolleeCSVFormatRow() + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
