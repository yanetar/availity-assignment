package com.yanet.lisp;

public class LispCodeChecker {

    public boolean checkParentheses(String code) {
        int openParentheses = 0;
        for (int i = 0; i < code.length() && openParentheses >=0; i++) {
            if (code.charAt(i) == '(') {
                openParentheses++;
            } else if (code.charAt(i) == ')') {
                openParentheses--;
            }
        }
        return openParentheses == 0;
    }
}
