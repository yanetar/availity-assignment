package com.yanet.enrollees;

import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class EnrollmentOrchestratorTest {

    private EnrollmentOrchestrator enrollmentOrchestrator =  new EnrollmentOrchestrator();

    @Test
    public void testEnrollmentOrchestrator() throws Exception{
        Path humana = Paths.get("./files/Humana.csv");
        Path uhc = Paths.get("./files/UnitedHealthcare.csv");
        Path florida = Paths.get("./files/FloridaBlue.csv");
        Files.deleteIfExists(humana);
        Files.deleteIfExists(uhc);
        Files.deleteIfExists(florida);
        enrollmentOrchestrator.processEnrollmentCsv("./files/EnrollmentFile.csv", "./files/");
        assertTrue(Files.exists(humana));
        assertTrue(Files.exists(uhc));
        assertTrue(Files.exists(florida));
    }

}