package com.yanet.enrollees;

import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class EnrollmentFileIoTest {

    private EnrollmentFileIo fileIo = new EnrollmentFileIo();

    @Test
    public void testOpenFile() throws Exception {
        List<Enrollee> enrollees = fileIo.readEnrollmentFile("./files/EnrollmentFile.csv");
        assertEquals(31, enrollees.size());
        assertEquals("000000001", enrollees.get(0).getUserId());
    }

    @Test
    public void testWriteFile() throws Exception {
        Path path = Paths.get("./files/Humana.csv");
        Files.deleteIfExists(path);
        Enrollee enrollee = new Enrollee(Arrays.asList("1","Anthony","Hopkins","1","Humana"));
        fileIo.writeEnrolleesInFile(Arrays.asList(enrollee), "./files/");
        assertTrue(Files.exists(path));
    }

}