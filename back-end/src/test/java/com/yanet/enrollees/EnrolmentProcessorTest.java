package com.yanet.enrollees;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class EnrolmentProcessorTest {

    private EnrolmentProcessor enrolmentProcessor = new EnrolmentProcessor();

    @Test
    public void testSortEnrolleesByLastAndFirstName(){
        Enrollee enrollee1 = new Enrollee(Arrays.asList("1","John","Hopkins","1","Humana"));
        Enrollee enrollee2 = new Enrollee(Arrays.asList("2","Tim","Bolton","1","Humana"));
        Enrollee enrollee3 = new Enrollee(Arrays.asList("3","Anthony","Hopkins","1","Humana"));
        List<Enrollee> enrollees = Arrays.asList(enrollee1, enrollee2, enrollee3);

        List<Enrollee> sortedEnrollees = enrolmentProcessor.sortEnrolleesByLastAndFirstName(enrollees);

        assertNotNull(sortedEnrollees);
        assertEquals(3, sortedEnrollees.size());
        assertEquals(enrollee2, sortedEnrollees.get(0));
        assertEquals(enrollee3, sortedEnrollees.get(1));
        assertEquals(enrollee1, sortedEnrollees.get(2));
    }

    @Test
    public void testRemoveDuplicateUserIds(){
        Enrollee enrollee1 = new Enrollee(Arrays.asList("1","John","Hopkins","1","Humana"));
        Enrollee enrollee2 = new Enrollee(Arrays.asList("2","Tim","Bolton","1","Humana"));
        Enrollee enrollee3 = new Enrollee(Arrays.asList("1","John","Hopkins","2","Humana"));
        List<Enrollee> enrollees = Arrays.asList(enrollee1, enrollee2, enrollee3);

        List<Enrollee> dedupEnrollees = enrolmentProcessor.removeDuplicateUserIds(enrollees);

        assertNotNull(dedupEnrollees);
        assertEquals(2, dedupEnrollees.size());
        assertFalse(dedupEnrollees.contains(enrollee1));
        assertTrue(dedupEnrollees.contains(enrollee2));
        assertTrue(dedupEnrollees.contains(enrollee3));
    }

    @Test
    public void testSplitByInsurance(){
        Enrollee enrollee1 = new Enrollee(Arrays.asList("1","Anthony","Hopkins","1","Humana"));
        Enrollee enrollee2 = new Enrollee(Arrays.asList("2","Tim","Bolton","1","Humana"));
        Enrollee enrollee3 = new Enrollee(Arrays.asList("1","John","Hopkins","1","UnitedHealthcare"));
        List<Enrollee> enrollees = Arrays.asList(enrollee1, enrollee2, enrollee3);
        Map<String, List<Enrollee>> byInsurance = enrolmentProcessor.splitByInsurance(enrollees);
        assertNotNull(byInsurance);
        assertEquals(2, byInsurance.size());
        List<Enrollee> humana = byInsurance.get("Humana");
        assertEquals(2, humana.size());
        assertTrue(humana.contains(enrollee1));
        assertTrue(humana.contains(enrollee2));
        List<Enrollee> unitedHealthcare = byInsurance.get("UnitedHealthcare");
        assertEquals(1, unitedHealthcare.size());
        assertTrue(unitedHealthcare.contains(enrollee3));
    }

}