package com.yanet.lisp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LispCodeCheckerTest {


    private LispCodeChecker lispCodeChecker = new LispCodeChecker();

    @Test
    public void testEmptyStringIsValid(){
        assertTrue(lispCodeChecker.checkParentheses(""));
    }

    @Test
    public void testSingleOpenIsInvalid(){
        assertFalse(lispCodeChecker.checkParentheses("("));
    }

    @Test
    public void testSingleCloseIsInvalid(){
        assertFalse(lispCodeChecker.checkParentheses(")"));
    }

    @Test
    public void testOpenAndCloseIsValid(){
        assertTrue(lispCodeChecker.checkParentheses("()"));
    }

    @Test
    public void testMoreOpenThanCloseIsInvalid(){
        assertFalse(lispCodeChecker.checkParentheses("(()"));
    }

    @Test
    public void testMoreCloseThanOpenIsInvalid(){
        assertFalse(lispCodeChecker.checkParentheses("())"));
    }

    @Test
    public void testCloseBeforeOpenIsInvalid(){
        assertFalse(lispCodeChecker.checkParentheses(")("));
    }

    @Test
    public void test2MoreOpenThanCloseIsInvalid(){
        assertFalse(lispCodeChecker.checkParentheses("(defun factorial (n)\n" +
                "    (if (= n 0) 1\n" +
                "        (* n (factorial (- n 1))))"));
    }

    @Test
    public void test2MoreCloseThanOpenIsInvalid(){
        assertFalse(lispCodeChecker.checkParentheses("(defun factorial (n)\n" +
                "    if (= n 0) 1\n" +
                "        (* n (factorial (- n 1)))))"));
    }
}